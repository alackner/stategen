#ifndef TESTPATHS_HPP
#define TESTPATHS_HPP


#include <string>
#include <filesystem>

class TestPaths
{
public:
    static std::filesystem::path GetTestOutputPath();
    static std::filesystem::path GetTestDataPath();
    
private:
    static void SetProjectRootPath();
    static std::filesystem::path ProjectRootPath;
};

#endif // TESTPATHS_HPP
