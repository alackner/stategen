#include "TestPaths.hpp"

#include <iostream>

std::filesystem::path TestPaths::ProjectRootPath;

std::filesystem::path TestPaths::GetTestOutputPath()
{
    if(ProjectRootPath.empty())
    {
        SetProjectRootPath();
    }

    auto rootPath = ProjectRootPath;
    return rootPath/"test"/"output";
}

std::filesystem::path TestPaths::GetTestDataPath()
{
    if(ProjectRootPath.empty())
    {
        SetProjectRootPath();
    }

    auto rootPath = ProjectRootPath;
    return rootPath/"test"/"data";
}

void TestPaths::SetProjectRootPath()
{
    auto currentDir = std::filesystem::current_path();
    
    while(!std::filesystem::exists(currentDir/"CMakeLists.txt") && currentDir.has_parent_path())
    {
        currentDir = currentDir.parent_path();
    }

    ProjectRootPath = currentDir;
}
