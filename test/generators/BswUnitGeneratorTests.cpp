#include "gtest/gtest.h"
#include "TestPaths.hpp"

#include "BswUnitGenerator.hpp"
#include "FileNotFoundException.hpp"
#include "Statemachine.hpp"

#include <memory>
#include <vector>
#include <filesystem>

class BswUnitGeneratorTests : public ::testing::Test
{
public:
    void SetUp() override
    {
        std::filesystem::create_directory(TestPaths::GetTestOutputPath());
    }

    void TearDown() override
    {
        //std::filesystem::remove_all(TestPaths::GetTestOutputPath());
    }

    Statemachine CreateEmptyStatemachine(std::string unitName)
    {
        Statemachine stm;
        stm.name = "BswUnitGeneratorTest";
        stm.unit = unitName;
        return stm;
    }

    Statemachine CreateSimpleStatemachine(std::string unitName)
    {
        Statemachine stm = CreateEmptyStatemachine(unitName);

        //States
        auto simpleState = std::make_shared<State>("Simple", StateType::Default);
        simpleState->actions.push_back(std::make_shared<Action>(ActionType::Entry, "SimpleEntryFunction"));
        auto initState = std::make_shared<State>("InitState", StateType::Initial);
        auto finalState = std::make_shared<State>("FinalState", StateType::Final);

        //Triggers
        auto processTrigger = std::make_shared<Trigger>("Process");

        //Transitions
        auto initTransition = std::make_shared<Transition>(initState, simpleState);
        initTransition->actions.push_back("StartJob");

        auto selfTransition = std::make_shared<Transition>(simpleState, simpleState);
        selfTransition->actions.push_back("SimpleAction");
        selfTransition->trigger = processTrigger;
        selfTransition->guard = "";

        auto finalTransition = std::make_shared<Transition>(simpleState, finalState);
        finalTransition->actions.push_back("EndJob");
        finalTransition->trigger = processTrigger;
        finalTransition->guard = "ReadyForFinal";

        //Wire the state machine together
        initState->transitions.push_back(initTransition);
        simpleState->transitions.push_back(selfTransition);
        simpleState->transitions.push_back(finalTransition);

        stm.states.push_back(simpleState);
        stm.states.push_back(initState);
        stm.states.push_back(finalState);

        stm.triggers.push_back(processTrigger);

        stm.transitions.push_back(initTransition);
        stm.transitions.push_back(finalTransition);

        return stm;
    }
};

TEST_F(BswUnitGeneratorTests, GroupStatemachines)
{
   BswUnitGenerator bswGenerator;   
   std::vector<Statemachine> statemachines;
   statemachines.push_back(CreateEmptyStatemachine("unitA"));
   statemachines.push_back(CreateEmptyStatemachine("unitB"));

   bswGenerator.Generate(statemachines, TestPaths::GetTestOutputPath().string());
}

TEST_F(BswUnitGeneratorTests, GenerateSimpleStatemachine)
{
   BswUnitGenerator bswGenerator;
   std::vector<Statemachine> statemachines;
   statemachines.push_back(CreateSimpleStatemachine("unitA"));
   
   bswGenerator.Generate(statemachines, TestPaths::GetTestOutputPath().string());
}
