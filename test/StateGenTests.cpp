#include "gtest/gtest.h"
#include "TestPaths.hpp"

#include "StateGen.hpp"

#include <vector>

class StateGenTests : public ::testing::Test
{
public:
    virtual void SetUp(){
        argBuffer.clear();
        argvBuffer.clear();
    }

    char** GetArgValues(std::vector<std::string> args)
    {
        argBuffer.reserve(args.size());
        argvBuffer.reserve(args.size());

        for(size_t i = 0; i < args.size(); ++i)
        {
            argBuffer.emplace_back(args[i].begin(), args[i].end());
            argBuffer.back().push_back('\0');
            argvBuffer.push_back(argBuffer.back().data());
        }

        return argvBuffer.data();
    }

    std::vector<std::vector<char>> argBuffer;
    std::vector<char*> argvBuffer;
};

TEST_F(StateGenTests, GenTest_Ea_Bsw)
{    
    StateGen stateGen;
    std::vector<std::string> arguments = {"", "-d" , (TestPaths::GetTestDataPath()/"TestData_EA.xml").string(), "-o", TestPaths::GetTestOutputPath().string(), "-p", "ea", "-g", "bsw"};

    int argc = arguments.size();
    char** argv = GetArgValues(arguments);

    stateGen.RunStateGen(argc, argv);

    ASSERT_EQ(1,1);
}
