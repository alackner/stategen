#include "unitAInternal.h"
#include "unitAMachine.h"

unitA_StateType unitA_SimpleState = {
	unitA_Simple_ProcessEvent,
}

void unitA_BswUnitGeneratorTest_Initialize(unitA_ContextType ctx)
{
	ctx->CurrentState = &unitA_SimpleState;
	unitA_Simple_OnEnter(ctx);
}

void unitA_Simple_OnEntry(unitA_ContextType ctx)
{
	SimpleEntryFunction(ctx);
}

void unitA_Simple_ProcessEvent(unitA_ContextType ctx)
{
	
}

