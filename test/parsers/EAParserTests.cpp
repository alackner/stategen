#include "gtest/gtest.h"
#include "TestPaths.hpp"

#include "EAParser.hpp"
#include "FileNotFoundException.hpp"

class EAParserTests : public ::testing::Test
{
public:

};

TEST_F(EAParserTests, ParseStatemachines_FileNotFound)
{
    EAParser parser;
    std::vector<Statemachine> statemachines;

    EXPECT_THROW(statemachines = parser.ParseModel("./invalidPath"), FileNotFoundException);
    EXPECT_EQ(statemachines.size(), 0);
}

TEST_F(EAParserTests, ParseStatemachines)
{
    EAParser parser;
    auto statemachines = parser.ParseModel(TestPaths::GetTestDataPath()/"TestData_EA.xml");

    ASSERT_EQ(statemachines.size(), 1);

    auto stm = *statemachines.begin();

    ASSERT_EQ(stm.states.size(), 4);
    ASSERT_EQ(stm.triggers.size(), 2);
    ASSERT_EQ(stm.transitions.size(), 5);
}