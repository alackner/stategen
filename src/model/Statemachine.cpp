#include "Statemachine.hpp"

#include <algorithm>

Statemachine::Statemachine()
{
}

Statemachine::~Statemachine()
{
}

std::string Statemachine::ToString()
{
    std::string stm;
    stm.append("Statemachine " + name + "\n\n");

    //States
    stm.append("<States>\n");
    for(auto state : states)
    {
        stm.append(state->name + "\n");

        for(auto action : state->actions)
        {
            stm.append("  " + action->GetType() + " " + action->functionName + "\n");
        }
    }

    stm.append("\n");

    //Triggers
    stm.append("<Triggers>\n");
    for(auto trigger : triggers)
    {
        stm.append(trigger->name + "\n");
    }

    stm.append("\n");

    //Transitions
    stm.append("<Transitions>\n");
    for(auto transition : transitions)
    {
        std::string combinedActions;
        std::for_each(transition->actions.begin(), transition->actions.end(), [&](std::string& action){ combinedActions += action + " "; });

        std::string triggerName = "";
        if(transition->trigger != nullptr)
        {
            triggerName = transition->trigger->name;
        }

        std::string trs = triggerName + "[" + transition->guard + "]/" + combinedActions;
        stm.append(transition->sourceState->name + " " + transition->targetState->name + " " + trs + " " + "\n");
    }

    stm.append("\n");

    return stm;
}