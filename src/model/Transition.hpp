#ifndef TRANSITION_HPP
#define TRANSITION_HPP

#include "Trigger.hpp"

#include <memory>
#include <vector>

class State;

using StatePtr = std::shared_ptr<State>;

class Transition
{
public:
    std::string id;
    std::shared_ptr<Trigger> trigger;
    std::string guard;
    std::vector<std::string> actions;

    std::shared_ptr<State> sourceState;
    std::shared_ptr<State> targetState;

    Transition();
    Transition(StatePtr source, StatePtr target);
    ~Transition();
};

#endif // TRANSITION_HPP
