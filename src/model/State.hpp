#ifndef STATE_HPP
#define STATE_HPP

#include "Transition.hpp"
#include "Action.hpp"

#include <memory>
#include <string>
#include <vector>

enum class StateType{
    Default,
    Initial,
    Final
};

class State
{
public:    
    StateType type;

    std::string id;
    std::string name;
    std::vector<std::shared_ptr<Transition>> transitions;
    std::vector<std::shared_ptr<Action>> actions;

    State();
    State(std::string stateName, StateType stateType);

    ~State();
};

#endif // STATE_HPP
