#ifndef TRIGGER_HPP
#define TRIGGER_HPP

#include <string>

class Trigger
{
public:
    std::string id;
    std::string name;

    Trigger(){};
    Trigger(std::string transitionName) : name(transitionName) {};
    ~Trigger(){};
};

#endif // TRIGGER_HPP
