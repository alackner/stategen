#ifndef ACTION_HPP
#define ACTION_HPP

#include <string>
#include <map>

enum class ActionType{
    Entry,
    Exit,
    Unknown
};

class Action
{
public:    
    std::string id;
    ActionType type;
    std::string functionName;

    Action(){};
    Action(ActionType actionType, std::string function) : type(actionType), functionName(function) {};
    
    ~Action(){};

    std::string GetType()
    {
        auto it = actionTypeMap.find(type);
    
        if (it != actionTypeMap.end()) {
            return it->second;
        } else { 
            return "";
        }
    }

private:
    std::map<ActionType, std::string> actionTypeMap = {
        {ActionType::Entry, "entry"},
        {ActionType::Exit, "exit"},
        {ActionType::Unknown, "unknown"}
    };
};

#endif // ACTION_HPP
