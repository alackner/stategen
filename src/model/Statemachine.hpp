#ifndef STATEMACHINE_HPP
#define STATEMACHINE_HPP

#include "State.hpp"
#include "Transition.hpp"
#include "Trigger.hpp"

#include <string>

class Statemachine
{
public:
    std::string id;
    std::string name;
    std::string unit;
    std::vector<std::shared_ptr<State>> states;
    std::vector<std::shared_ptr<Trigger>> triggers;
    std::vector<std::shared_ptr<Transition>> transitions;

    Statemachine();
    ~Statemachine();

    std::string ToString();
};

#endif // STATEMACHINE_HPP
