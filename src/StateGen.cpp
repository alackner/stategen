#include "StateGen.hpp"
#include "GeneratorFactory.hpp"
#include "ParserFactory.hpp"

StateGen::StateGen()
{
    argparser = std::make_shared<ArgumentParser>("Generates a state machine implementation based on a given design.");

    argparser->addArgument("-d", "Path to the design file that contains the state machine to generate.", true);
    argparser->addArgument("-p", "Parser to use. Currently 'ea' and 'puml' are supported.", true);
    argparser->addArgument("-g", "Generator to use. Currently 'bsw' is supported.", true);
    argparser->addArgument("-o", "Output directory the state machine is generated to.", true);
}

StateGen::~StateGen()
{
    
}

void StateGen::RunStateGen(int argc, char** argv)
{
    argparser->parse(argc, argv);
    auto parserType = argparser->getArgument("-p");
    auto parser = ParserFactory::CreateStatemachineParser(parserType.value());

    if(parser.has_value())
    {
        auto designPath = argparser->getArgument("-d").value();
        auto stateModel = parser.value()->ParseModel(designPath);

        auto generatorType = argparser->getArgument("-g");
        auto generator = GeneratorFactory::CreateStatemachineGenerator(generatorType.value());

        if(generator.has_value())
        {
            auto outputPath = argparser->getArgument("-o");
            generator.value()->Generate(stateModel, outputPath.value());
        }
    }
}