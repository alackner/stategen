#ifndef BSWUNITGENERATOR_HPP
#define BSWUNITGENERATOR_HPP

#include "StatemachineGenerator.hpp"

#include <string>
#include <vector>
#include <tuple>
#include <optional>

class BswUnitGenerator : public StatemachineGenerator
{
public:
    BswUnitGenerator();
    ~BswUnitGenerator();

    virtual void Generate(std::vector<Statemachine> statemachines, std::string outputPath);
    
private:
    std::vector<std::string> StateDefinitions;
    std::vector<std::string> StateMachineFunctions;
    std::vector<std::string> ActionFunctions;

    std::map<std::string, std::vector<Statemachine>> GroupByUnit(std::vector<Statemachine> statemachines);
    std::map<std::shared_ptr<Trigger>, std::vector<std::shared_ptr<Transition>>> GroupByTrigger(std::shared_ptr<State> state);

    void GenerateUnit(std::string unitName, std::vector<Statemachine> statemachines, std::string outputPath);
    std::vector<std::string> GenerateStateDefinitions(std::string unitName, Statemachine stm);

    std::vector<std::string> GenerateMachineFunctions(std::string unitName, Statemachine stm);
    std::string GenerateInitFunction(std::string unitName, Statemachine stm);
    std::string GenerateEventHandler(std::string unitName, std::shared_ptr<State> state, std::tuple<std::shared_ptr<Trigger>, std::vector<std::shared_ptr<Transition>>> trigger);
    std::string GenerateStateAction(std::string unitName, std::shared_ptr<State> state, std::shared_ptr<Action> action);
    std::optional<std::string> GenerateOnEnterCall(std::string unitName, std::shared_ptr<Transition> transition);
    std::optional<std::string> GenerateOnExitCall(std::string unitName, std::shared_ptr<Transition> transition);

    std::vector<std::string> GenerateActionFunctions(std::string unitName, Statemachine stm);

    void GeneratedOutputFiles(std::string unitName, std::string outputPath);
};

#endif // BSWUNITGENERATOR_HPP
