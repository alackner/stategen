#include "GeneratorFactory.hpp"
#include "BswUnitGenerator.hpp"

GeneratorFactory::GeneratorFactory()
{
}

GeneratorFactory::~GeneratorFactory()
{
}

std::optional<std::shared_ptr<StatemachineGenerator>> GeneratorFactory::CreateStatemachineGenerator(std::string generatorType)
{
    if(generatorType == "bsw")
    {
        return std::make_shared<BswUnitGenerator>();
    }

    return std::nullopt;
}