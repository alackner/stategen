#include "BswUnitGenerator.hpp"
#include "State.hpp"
#include "Statemachine.hpp"

#include <iostream>
#include <fstream>
#include <map>
#include <utility>
#include <sstream>
#include <algorithm>

BswUnitGenerator::BswUnitGenerator()
{
}

BswUnitGenerator::~BswUnitGenerator()
{
}

void BswUnitGenerator::Generate(std::vector<Statemachine> statemachines, std::string outputPath)
{
    auto unitStatemachines = GroupByUnit(statemachines);

    for(auto unitGroup : unitStatemachines)
    {
        GenerateUnit(unitGroup.first, unitGroup.second, outputPath);
    }
}

std::map<std::string, std::vector<Statemachine>> BswUnitGenerator::GroupByUnit(std::vector<Statemachine> statemachines)
{
    std::map<std::string, std::vector<Statemachine>> unitStatemachines;

    for(auto& stm : statemachines)
    {
        if(unitStatemachines.find(stm.unit) == unitStatemachines.end())
        {
            unitStatemachines.insert(std::pair(stm.unit, std::vector<Statemachine>()));
        }

        unitStatemachines[stm.unit].push_back(stm);
    }

    return unitStatemachines;
}

std::map<std::shared_ptr<Trigger>, std::vector<std::shared_ptr<Transition>>> BswUnitGenerator::GroupByTrigger(std::shared_ptr<State> state)
{
    std::map<std::shared_ptr<Trigger>, std::vector<std::shared_ptr<Transition>>> groupedTriggers;

    for(auto& transition : state->transitions)
    {
        if(groupedTriggers.find(transition->trigger) == groupedTriggers.end())
        {
            groupedTriggers.insert(std::pair(transition->trigger, std::vector<std::shared_ptr<Transition>>()));
        }

        groupedTriggers[transition->trigger].push_back(transition);
    }

    return groupedTriggers;
}

void BswUnitGenerator::GenerateUnit(std::string unitName, std::vector<Statemachine> statemachines, std::string outputPath)
{
    for(auto stm : statemachines)
    {
        auto stmStates = GenerateStateDefinitions(unitName, stm);
        auto stmFunctions = GenerateMachineFunctions(unitName, stm);
        auto actionFunctions = GenerateActionFunctions(unitName, stm);

        StateDefinitions.insert(StateDefinitions.end(), stmStates.begin(), stmStates.end());
        StateMachineFunctions.insert(StateMachineFunctions.end(), stmFunctions.begin(), stmFunctions.end());
        ActionFunctions.insert(ActionFunctions.end(), actionFunctions.begin(), actionFunctions.end());
    }

    GeneratedOutputFiles(unitName, outputPath);
}

std::vector<std::string> BswUnitGenerator::GenerateStateDefinitions(std::string unitName, Statemachine stm)
{
    std::vector<std::string> stmStates;
    std::string unitStateType = unitName + "_StateType";

    //Generate default state
    std::stringstream defaultState;
    defaultState << unitStateType << " " << unitName << "_DefaultState = {" << std::endl;

    for(auto& trigger : stm.triggers)
    {
        defaultState << "\t" << unitName << "_DefaultEvent" << "," << std::endl;
    }

    defaultState << "}" << std::endl << std::endl;
    stmStates.push_back(defaultState.str());

    //Generate state machine states
    for(auto state : stm.states)
    {
        if(state->type == StateType::Default)
        {
            std::stringstream genState;
            genState << unitStateType << " " << unitName << "_" << state->name << "State = {" << std::endl;

            for(auto trigger : stm.triggers)
            {
                auto stateTransitionIt = std::find_if(state->transitions.begin(), state->transitions.end(), [&](std::shared_ptr<Transition> tr){
                    return tr->trigger->name == trigger->name;
                });

                if(stateTransitionIt != state->transitions.end())
                {
                    genState << "\t" << unitName << "_" << state->name << "_" << trigger->name << "Event" << "," << std::endl;
                }
                else
                {
                    genState << "\t" << unitName << "_DefaultEvent" << "," << std::endl;
                }
                
            }

            genState << "}" << std::endl << std::endl;
            stmStates.push_back(genState.str());
        }
    }

    return stmStates;
}

std::vector<std::string> BswUnitGenerator::GenerateMachineFunctions(std::string unitName, Statemachine stm)
{
    std::vector<std::string> stmFunctions;

    //Generate state machine init function
    stmFunctions.push_back(GenerateInitFunction(unitName, stm));

    //Generate state functions
    for(auto& state : stm.states)
    {
        if(state->type == StateType::Default)
        {
            for(auto& action : state->actions)
            {
                stmFunctions.push_back(GenerateStateAction(unitName, state, action));
            }

            auto groupedTriggers = GroupByTrigger(state);
            for(auto& triggerTuple : groupedTriggers)
            {
                if(triggerTuple.first != nullptr)
                {
                    stmFunctions.push_back(GenerateEventHandler(unitName, state, std::make_tuple(triggerTuple.first, triggerTuple.second)));
                }
            }
        }
    }

    return stmFunctions;
}

std::string BswUnitGenerator::GenerateInitFunction(std::string unitName, Statemachine stm)
{
    std::stringstream initFunction;

    auto initStateIt = std::find_if(stm.states.begin(), stm.states.end(), [](std::shared_ptr<State>& state){
        return state->type == StateType::Initial;
    });
        
    initFunction << "void " << unitName << "_" << stm.name << "_Initialize(" << unitName << "_ContextType ctx)" << std::endl;
    initFunction << "{" << std::endl;

    if(initStateIt != stm.states.end() && (*initStateIt)->transitions.size() > 0)
    {
        initFunction << "\t" << "ctx->CurrentState = &" << unitName << "_" << (*initStateIt)->transitions[0]->targetState->name << "State;" << std::endl;

        auto onEnterCall = GenerateOnEnterCall(unitName, (*initStateIt)->transitions[0]);
        if(onEnterCall.has_value())
        {
            initFunction << "\t" << onEnterCall.value() << std::endl;
        }
    }
    else
    {
        initFunction << "\t#error No init transition found!" << std::endl;   
    }

    initFunction << "}" << std::endl;

    return initFunction.str();
}

std::string BswUnitGenerator::GenerateEventHandler(std::string unitName, std::shared_ptr<State> state, std::tuple<std::shared_ptr<Trigger>, std::vector<std::shared_ptr<Transition>>> trigger)
{
    std::stringstream eventHandlerFunction;
    std::string indention = "\t";

    eventHandlerFunction << "void " << unitName << "_" << state->name << "_" << std::get<0>(trigger)->name << "Event(" << unitName << "_ContextType ctx)" << std::endl;
    eventHandlerFunction << "{" << std::endl;

    std::sort(std::get<1>(trigger).begin(), std::get<1>(trigger).end());
    std::reverse(std::get<1>(trigger).begin(), std::get<1>(trigger).end());

    for(auto transition : std::get<1>(trigger))
    {
        if(!transition->guard.empty())
        {
            indention = "\t\t";

            eventHandlerFunction << "\tif(" << unitName << "_Is" << transition->guard << "(ctx))" << std::endl;
            eventHandlerFunction << "\t{" << std::endl;
        }        
        
        //Change state
        if(transition->targetState->type == StateType::Default)
        {
            eventHandlerFunction << indention << "ctx->CurrentState = &" << unitName << "_" << transition->targetState->name << "State;" << std::endl;
        }
        else
        {
            eventHandlerFunction << indention << "ctx->CurrentState = &" << unitName << "_DefaultState;" << std::endl;
            
        }        

        //Exit current state
        auto onExitCall = GenerateOnExitCall(unitName, transition);
        if(onExitCall.has_value())
        {
            eventHandlerFunction << indention << onExitCall.value() << std::endl;
        }

        //Execute transition actions
        for(auto transitionAction : transition->actions)
        {
            eventHandlerFunction << indention << transitionAction << "(ctx);" << std::endl;
        }

        //Enter successor state
        auto onEnterCall = GenerateOnEnterCall(unitName, transition);
        if(onEnterCall.has_value())
        {
            eventHandlerFunction << indention << onEnterCall.value() << std::endl;
        }

        if(!transition->guard.empty())
        {
            eventHandlerFunction << "\t}" << std::endl << std::endl;
        }

        indention = "\t";
    }

    eventHandlerFunction << "}" << std::endl;

    return eventHandlerFunction.str();
}

std::string BswUnitGenerator::GenerateStateAction(std::string unitName, std::shared_ptr<State> state, std::shared_ptr<Action> action)
{
    std::stringstream stateActionFunction;
    std::string actionType = action->GetType();
    actionType[0] = toupper(actionType[0]);

    stateActionFunction << "void " << unitName << "_" << state->name << "_On" << actionType << "(" << unitName << "_ContextType ctx)" << std::endl;
    stateActionFunction << "{" << std::endl;
    stateActionFunction << "\t" << action->functionName << "(ctx);" << std::endl;
    stateActionFunction << "}" << std::endl;

    return stateActionFunction.str();
}

std::optional<std::string> BswUnitGenerator::GenerateOnEnterCall(std::string unitName, std::shared_ptr<Transition> transition)
{
    auto targetOnEnterActionIt = std::find_if(transition->targetState->actions.begin(),transition->targetState->actions.end(), [](std::shared_ptr<Action>& action){
        return action->type == ActionType::Entry;
    });

    if(targetOnEnterActionIt != transition->targetState->actions.end())
    {
        return unitName + "_" + transition->targetState->name + "_OnEnter(ctx);";
    }

    return std::nullopt;
}

std::optional<std::string> BswUnitGenerator::GenerateOnExitCall(std::string unitName, std::shared_ptr<Transition> transition)
{
    auto targetOnExitActionIt = std::find_if(transition->sourceState->actions.begin(),transition->sourceState->actions.end(), [](std::shared_ptr<Action>& action){
        return action->type == ActionType::Exit;
    });

    if(targetOnExitActionIt != transition->sourceState->actions.end())
    {
        return unitName + "_" + transition->sourceState->name + "_OnExit(ctx);";
    }

    return std::nullopt;
}

std::vector<std::string> BswUnitGenerator::GenerateActionFunctions(std::string unitName, Statemachine stm)
{
    std::vector<std::string> actionFunctions;

    //State actions
    for(auto& state : stm.states)
    {
        for(auto& action : state->actions)
        {
            std::stringstream actionStream;        
            actionStream << "void " << unitName << "_" << action->functionName << "(" << unitName << "_ContextType ctx)" << std::endl;
            actionStream << "{" << std::endl;
            actionStream << "\t" << std::endl;
            actionStream << "}" << std::endl;
            actionFunctions.push_back(actionStream.str());
        }
    }

    //Transition actions
    for(auto& transition : stm.transitions)
    {
        for(auto& action : transition->actions)
        {
            std::stringstream actionStream;        
            actionStream << "void " << unitName << "_" << action << "(" << unitName << "_ContextType ctx)" << std::endl;
            actionStream << "{" << std::endl;
            actionStream << "\t" << std::endl;
            actionStream << "}" << std::endl;
            actionFunctions.push_back(actionStream.str());
        }
    }

    return actionFunctions;
}

void BswUnitGenerator::GeneratedOutputFiles(std::string unitName, std::string outputPath)
{   
    //Unit internal header
    std::ofstream unitInternalHeader;
    unitInternalHeader.open(outputPath + "/" + unitName + "Internal.h");

    for(auto function : ActionFunctions)
    {
        auto functionSignature = function.substr(0, function.find("\n")) + ";";
        unitInternalHeader << functionSignature << std::endl << std::endl;
    }

    unitInternalHeader.close();

    //Unit machine header
    std::ofstream unitMachineHeader;
    unitMachineHeader.open(outputPath + "/" + unitName + "Machine.h");

    unitMachineHeader << "#include \"" << unitName << "Internal.h\"" << std::endl;
    unitMachineHeader << std::endl;

    for(auto function : StateMachineFunctions)
    {
        auto functionSignature = function.substr(0, function.find("\n")) + ";";
        unitMachineHeader << functionSignature << std::endl << std::endl;
    }

    unitMachineHeader.close();

    //Unit machine source
    std::ofstream unitMachineSource;
    unitMachineSource.open(outputPath + "/" + unitName + "Machine.c");

    unitMachineSource << "#include \"" << unitName << "Internal.h\"" << std::endl;
    unitMachineSource << "#include \"" << unitName << "Machine.h\"" << std::endl;
    unitMachineSource << std::endl;

    for(auto state : StateDefinitions)
    {
        unitMachineSource << state;
    }

    for(auto function : StateMachineFunctions)
    {
        unitMachineSource << function;
        unitMachineSource << std::endl;
    }

    unitMachineSource.close();
}
