#ifndef STATEMACHINEGENERATOR_HPP
#define STATEMACHINEGENERATOR_HPP

#include "Statemachine.hpp"

#include <string>

class StatemachineGenerator
{
private:
    
public:
    StatemachineGenerator();
    ~StatemachineGenerator();

    virtual void Generate(std::vector<Statemachine> statemachines, std::string outputPath) = 0;
};

#endif // STATEMACHINEGENERATOR_HPP
