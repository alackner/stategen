#ifndef GENERATORFACTORY_HPP
#define GENERATORFACTORY_HPP

#include "StatemachineGenerator.hpp"

#include <memory>
#include <optional>
#include <string>

class GeneratorFactory
{
private:
    GeneratorFactory();
    ~GeneratorFactory();

public:
    static std::optional<std::shared_ptr<StatemachineGenerator>> CreateStatemachineGenerator(std::string generatorType);
};

#endif // GENERATORFACTORY_HPP
