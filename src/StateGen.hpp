#ifndef STATEGEN_HPP
#define STATEGEN_HPP

#include "ArgumentParser.hpp"

#include <memory>

class StateGen
{
private:
    std::shared_ptr<ArgumentParser> argparser;

public:
    StateGen();
    ~StateGen();
    
    void RunStateGen(int argc, char** argv);
};

#endif // STATEGEN_HPP
