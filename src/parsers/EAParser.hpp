#ifndef EAPARSER_HPP
#define EAPARSER_HPP

#include "StatemachineParser.hpp"
#include "pugixml.hpp"

#include <optional>

class EAParser : public StatemachineParser
{
private:
    Statemachine ParseStatemachine(pugi::xpath_node stmNode);
    std::shared_ptr<State> ParseState(pugi::xpath_node stmNode);
    std::shared_ptr<Trigger> ParseTrigger(pugi::xpath_node trNode);
    std::shared_ptr<State> FindInitial(std::string stmId, pugi::xpath_node stmNode);
    std::shared_ptr<State> FindFinal(std::string stmId, pugi::xpath_node stmNode);

    void ConnectStates(Statemachine& stm, pugi::xpath_node stmNode);
    std::shared_ptr<Transition> ParseTransition(Statemachine& stm, pugi::xpath_node trsNode);

    std::optional<std::shared_ptr<State>> FindState(Statemachine& stm, std::string stateId);
    ActionType MapActionType(std::string typeName);

public:
    EAParser();
    ~EAParser();

    virtual std::vector<Statemachine> ParseModel(std::filesystem::path sourceFile);
};

#endif // EAPARSER_HPP
