#ifndef PARSERFACTORY_HPP
#define PARSERFACTORY_HPP

#include "StatemachineParser.hpp"

#include <memory>
#include <optional>

class ParserFactory
{
private:
    ParserFactory();
    ~ParserFactory();

public:
    static std::optional<std::shared_ptr<StatemachineParser>> CreateStatemachineParser(std::string parserType);
};

#endif // PARSERFACTORY_HPP
