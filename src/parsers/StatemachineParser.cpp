#include "StatemachineParser.hpp"

#include <fstream>

StatemachineParser::~StatemachineParser()
{

}

bool StatemachineParser::FileExists(std::filesystem::path file)
{
    return std::filesystem::exists(file);
}