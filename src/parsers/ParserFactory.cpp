#include "ParserFactory.hpp"
#include "EAParser.hpp"

ParserFactory::ParserFactory()
{
}

ParserFactory::~ParserFactory()
{
}

std::optional<std::shared_ptr<StatemachineParser>> ParserFactory::CreateStatemachineParser(std::string parserType)
{
    if(parserType == "ea")
    {
        return std::make_shared<EAParser>();
    }

    return std::nullopt;
}