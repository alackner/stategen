#include "EAParser.hpp"
#include "FileNotFoundException.hpp"

#include <iostream>
#include <map>
#include <algorithm>
#include <optional>

std::map<std::string, ActionType> actionTypeMap = {
    {"entry", ActionType::Entry},
    {"exit", ActionType::Exit}
};

EAParser::EAParser()
{
}

EAParser::~EAParser()
{
}

std::vector<Statemachine> EAParser::ParseModel(std::filesystem::path sourceFile)
{
    std::vector<Statemachine> statemachines;

    if(!FileExists(sourceFile))
    {
        std::cout << "Input file for parsing not found at " << sourceFile << std::endl;
        throw FileNotFoundException();
    }
    else
    {
        pugi::xml_document doc;
        pugi::xml_parse_result result = doc.load_file(sourceFile.c_str());
        
        pugi::xpath_node_set stmNodes = doc.select_nodes("//packagedElement[@xmi:type='uml:StateMachine']");
        
        for(auto stmNode : stmNodes)
        {
            auto stm = ParseStatemachine(stmNode);
            statemachines.push_back(stm);
        }        
    }   

    return statemachines;
}

Statemachine EAParser::ParseStatemachine(pugi::xpath_node stmNode)
{
    Statemachine stm;
    stm.id = stmNode.node().attribute("xmi:id").value();
    stm.name = stmNode.node().attribute("name").value();
    stm.unit = "TestUnit";

    //Parse states
    auto stateQuery = "//packagedElement[@xmi:id='" + stm.id + "']//subvertex[@xmi:type='uml:State']";
    auto stateNodes = stmNode.node().select_nodes(stateQuery.c_str());
    for(auto stateNode : stateNodes)
    {
        stm.states.push_back(ParseState(stateNode));
    }

    //Parse triggers
    auto triggerQuery = "//packagedElement[@xmi:id='" + stm.id + "']//nestedClassifier[@xmi:type='uml:Trigger']";
    auto triggerNodes = stmNode.node().select_nodes(triggerQuery.c_str());
    for(auto triggerNode : triggerNodes)
    {
        stm.triggers.push_back(ParseTrigger(triggerNode));
    }

    //Find edge states
    auto initialState = FindInitial(stm.id, stmNode);
    stm.states.push_back(initialState);

    auto finalState = FindFinal(stm.id, stmNode);
    stm.states.push_back(finalState);

    //Parse transitions and connect the states
    ConnectStates(stm, stmNode);

    return stm;
}

std::shared_ptr<State> EAParser::ParseState(pugi::xpath_node stNode)
{
    auto st = std::make_shared<State>();

    st->type = StateType::Default;
    st->id = stNode.node().attribute("xmi:id").value();
    st->name = stNode.node().attribute("name").value();

    //Parse actions
    auto actionQuery = "//subvertex[@xmi:id='" + st->id + "']//*[@xmi:type='uml:Behavior']";
    auto actions = stNode.node().select_nodes(actionQuery.c_str());

    for(auto actionNode : actions)
    {
        auto action = std::make_shared<Action>();
        auto actionType = actionNode.node().name();
        auto operationQuery = "//subvertex[@xmi:id='" + st->id + "']//ownedOperation";
     
        action->type = MapActionType(actionType);
        action->id = actionNode.node().attribute("xmi:id").value();
        action->functionName = actionNode.node().select_node(operationQuery.c_str()).node().attribute("name").value();

        st->actions.push_back(action);
    }

    return st;
}

std::shared_ptr<Trigger> EAParser::ParseTrigger(pugi::xpath_node trNode)
{
    auto tr = std::make_shared<Trigger>();

    tr->id = trNode.node().attribute("xmi:id").value();
    tr->name = trNode.node().attribute("name").value();

    return tr;
}

std::shared_ptr<State> EAParser::FindInitial(std::string stmId, pugi::xpath_node stmNode)
{
    auto initState = std::make_shared<State>();

    auto initialQuery = "//packagedElement[@xmi:id='" + stmId + "']//subvertex[@xmi:type='uml:Pseudostate'][@kind='initial']";
    auto initialNode = stmNode.node().select_node(initialQuery.c_str());

    if(initialNode != nullptr)
    {
        initState->type = StateType::Initial;
        initState->id = initialNode.node().attribute("xmi:id").value();
        initState->name = initialNode.node().attribute("name").value();
    }

    return initState;
}

std::shared_ptr<State> EAParser::FindFinal(std::string stmId, pugi::xpath_node stmNode)
{
    auto finalState = std::make_shared<State>();

    auto finalQuery = "//packagedElement[@xmi:id='" + stmId + "']//subvertex[@xmi:type='uml:FinalState']";
    auto finalNode = stmNode.node().select_node(finalQuery.c_str());

    if(finalNode != nullptr)
    {
        finalState->type = StateType::Final;
        finalState->id = finalNode.node().attribute("xmi:id").value();
        finalState->name = finalNode.node().attribute("name").value();
    }

    return finalState;
}

void EAParser::ConnectStates(Statemachine& stm, pugi::xpath_node stmNode)
{
    auto transitionQuery = "//packagedElement[@xmi:id='" + stm.id + "']//transition[@xmi:type='uml:Transition']";
    auto transitionNodes = stmNode.node().select_nodes(transitionQuery.c_str());

    for(auto transitionNode : transitionNodes)
    {
        auto trs = ParseTransition(stm, transitionNode);

        auto sourceStateOpt = FindState(stm, std::string(transitionNode.node().attribute("source").value()));
        auto targetStateOpt = FindState(stm, std::string(transitionNode.node().attribute("target").value()));

        if(sourceStateOpt.has_value()) 
        {
            auto sourceState = sourceStateOpt.value();
            trs->sourceState = sourceState;
            sourceState->transitions.push_back(trs);
        }

        if(targetStateOpt.has_value())
        {
            auto targetState = targetStateOpt.value();
            trs->targetState = targetState;
            targetState->transitions.push_back(trs);
        }

        stm.transitions.push_back(trs);
    }
}

std::shared_ptr<Transition> EAParser::ParseTransition(Statemachine& stm, pugi::xpath_node trsNode)
{
    auto trs = std::make_shared<Transition>();
    trs->id = trsNode.node().attribute("xmi:id").value();

    //Parse transition trigger
    auto trsTriggerQuery = "//transition[@xmi:id='" + trs->id + "']//trigger";
    auto trsTriggerNode = trsNode.node().select_node(trsTriggerQuery.c_str());

    if(trsTriggerNode != nullptr)
    {
        auto triggerRef = trsTriggerNode.node().attribute("xmi:idref").value();
        auto triggerPtr = std::find_if(stm.triggers.begin(), stm.triggers.end(), [&](const std::shared_ptr<Trigger>& trigger){
            return trigger->id == triggerRef;
        });

        if(triggerPtr != stm.triggers.end())
        {
            trs->trigger = *triggerPtr;
        }
    }

    //Parse transition guard
    auto trsGuardSpecQuery = "//transition[@xmi:id='" + trs->id + "']//specification";
    auto trsGuardSpecNode = trsNode.node().select_node(trsGuardSpecQuery.c_str());

    if(trsGuardSpecNode != nullptr)
    {
        trs->guard = trsGuardSpecNode.node().attribute("body").value();        
    }

    //Parse transition actions
    auto trsEffectQuery = "//transition[@xmi:id='" + trs->id + "']//effect";
    auto trsEffectNode = trsNode.node().select_node(trsEffectQuery.c_str());

    if(trsEffectNode != nullptr)
    {
        auto actions = std::string(trsEffectNode.node().attribute("body").value());
        auto delimiter = std::string("\n");

        size_t pos = 0;
        std::string token;

        while ((pos = actions.find(delimiter)) != std::string::npos) {
            trs->actions.push_back(actions.substr(0, pos));
            actions.erase(0, pos + delimiter.length());
        }

        trs->actions.push_back(actions);
    }

    return trs;
}

std::optional<std::shared_ptr<State>> EAParser::FindState(Statemachine& stm, std::string stateId)
{
    auto statePtr = std::find_if(stm.states.begin(), stm.states.end(), [&](const std::shared_ptr<State>& state){
        return state->id == stateId;
    });

    if(statePtr != stm.states.end())
    {
        return std::optional<std::shared_ptr<State>>{*statePtr};
    }

    return std::nullopt;
}

ActionType EAParser::MapActionType(std::string typeName)
{
    auto it = actionTypeMap.find(typeName);
    
    if (it != actionTypeMap.end()) {
        return it->second;
    } else { 
        return ActionType::Unknown;
    }
}