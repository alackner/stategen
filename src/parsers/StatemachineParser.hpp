#ifndef STATEMACHINEPARSER_HPP
#define STATEMACHINEPARSER_HPP

#include "Statemachine.hpp"

#include <string>
#include <vector>
#include <filesystem>

class StatemachineParser
{
public:
    virtual ~StatemachineParser();
    virtual std::vector<Statemachine> ParseModel(std::filesystem::path sourceFile) = 0;

protected:
    bool FileExists(std::filesystem::path file);
};

#endif // STATEMACHINEPARSER_HPP
