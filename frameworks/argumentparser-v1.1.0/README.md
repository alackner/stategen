[![pipeline status](https://gitlab.com/alackner/argumentparser/badges/master/pipeline.svg)](https://gitlab.com/alackner/argumentparser/-/commits/master)

# ArgumentParer
Library to add a command line interface to an application.

## Usage
Make an object of the `ArgumentParser` class and add parameters. Afterwards simply call the `parse` function with argc and argv as arguments.

```
int main(int argc, char **argv) 
{
    ArgumentParser argParser("MyArgumentParser");
    
    argParser.addArgument("-t", "This is a test parameter");
    
    argParser.parse(argc, argv);
}
```

## Features
### Generating help
The `-h` argument is already build in. If the executable is executed with `-h` help is printed to the command line.

### Required arguments
An argument can be added with the option `required=true`. If no value is provided for this argument the program exits.

### Configure info stream and exit function
With the optional constructor `ArgumentParser(std::string description, std::ostream& infoStream, std::function<void()> exitFunction)` 
the target stream where the output is printed to as well as the function that is called in case the parser wants to exit can be modified.