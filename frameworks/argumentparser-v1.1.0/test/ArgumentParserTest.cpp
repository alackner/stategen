#include "ArgumentParser.hpp"

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <algorithm>
#include <gmock/gmock-spec-builders.h>
#include <ostream>
#include <vector>

class ArgumentParserTest : public ::testing::Test
{
public:
    ArgumentParserTest(){};
    
    ~ArgumentParserTest()
    {
        argBuffer.clear();
        argvBuffer.clear();
    };

    char** GetArgValues(std::vector<std::string> args)
    {
        argBuffer.reserve(args.size());
        argvBuffer.reserve(args.size());

        for(size_t i = 0; i < args.size(); ++i)
        {
            argBuffer.emplace_back(args[i].begin(), args[i].end());
            argBuffer.back().push_back('\0');
            argvBuffer.push_back(argBuffer.back().data());
        }

        return argvBuffer.data();
    }

    std::vector<std::vector<char>> argBuffer;
    std::vector<char*> argvBuffer;
};

TEST_F(ArgumentParserTest, SingleParameterTest)
{
    ArgumentParser argParser;
    std::vector<std::string> arguments = {"ArgumentParserTest", "-t" ,"Test"};

    argParser.addArgument("-t");

    int argc = arguments.size();
    char** argv = GetArgValues(arguments);

    argParser.parse(argc, argv);

    EXPECT_EQ(argParser.getArgument("-t"), "Test");
}

TEST_F(ArgumentParserTest, MultipleParametersTest)
{
    ArgumentParser argParser;
    std::vector<std::string> arguments = {"ArgumentParserTest", "-f" ,"foo", "-b", "bar"};

    argParser.addArgument("-b");
    argParser.addArgument("-f");

    int argc = arguments.size();
    char** argv = GetArgValues(arguments);

    argParser.parse(argc, argv);

    EXPECT_EQ(argParser.getArgument("-f"), "foo");
    EXPECT_EQ(argParser.getArgument("-b"), "bar");
}

TEST_F(ArgumentParserTest, MissingRequiredParameterTest)
{
    std::stringstream outStream;
    auto mockExit = testing::MockFunction<void()>();
    ArgumentParser argParser("", outStream, mockExit.AsStdFunction());

    std::vector<std::string> arguments = {"ArgumentParserTest", "-f" ,"foo"};

    argParser.addArgument("-f");
    argParser.addArgument("-r", "", true);

    int argc = arguments.size();
    char** argv = GetArgValues(arguments);

    EXPECT_CALL(mockExit, Call());

    argParser.parse(argc, argv);
}


TEST_F(ArgumentParserTest, NoValueForParamTest)
{
    std::stringstream outStream;
    auto mockExit = testing::MockFunction<void()>();
    ArgumentParser argParser("", outStream, mockExit.AsStdFunction());

    std::vector<std::string> arguments = {"ArgumentParserTest", "-f" ,"foo", "-b"};

    argParser.addArgument("-b");
    argParser.addArgument("-f");

    int argc = arguments.size();
    char** argv = GetArgValues(arguments);

    EXPECT_CALL(mockExit, Call());

    argParser.parse(argc, argv);

    EXPECT_EQ(argParser.getArgument("-f"), "foo");
    EXPECT_EQ(outStream.str(), "No value found for parameter -b.\n");      
}

TEST_F(ArgumentParserTest, PrintHelpTest)
{
    std::stringstream outStream;
    auto mockExit = testing::MockFunction<void()>();
    ArgumentParser argParser("MyTestCLI", outStream, mockExit.AsStdFunction());

    std::vector<std::string> arguments = {"ArgumentParserTest", "-h"};

    argParser.addArgument("-f", "foo");
    argParser.addArgument("-b", "bar");
    
    int argc = arguments.size();
    char** argv = GetArgValues(arguments);

    EXPECT_CALL(mockExit, Call());

    argParser.parse(argc, argv);

    EXPECT_EQ(outStream.str(), "MyTestCLI\n\n-f\tfoo\n-b\tbar\n");      
}