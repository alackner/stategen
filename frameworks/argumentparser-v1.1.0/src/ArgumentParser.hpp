#ifndef ARGUMENTPARSER_HPP
#define ARGUMENTPARSER_HPP

#include "Parameter.hpp"

#include <string>
#include <vector>
#include <optional>
#include <iostream>
#include <functional>
#include <map>

class ArgumentParser{
public:
    ArgumentParser();
    ArgumentParser(std::string description);
    ArgumentParser(std::string description, std::ostream& infoStream, std::function<void()> exitFunction);
    
    ~ArgumentParser();

    void addArgument(std::string parameter, std::string help = "", bool required = false);
    void parse(int argc, char** argv);

    std::optional<std::string> getArgument(std::string parameter);

private:
    std::string description;
    std::ostream& infoStream;
    std::function<void()> exitFunction;

    std::vector<Parameter> parameters;

    std::map<std::string, std::string> parsedArguments;

    void checkIfRequiredExist();
    void printHelp();
};

#endif // ARGUMENTPARSER_HPP
