#ifndef PARAMETER_HPP
#define PARAMETER_HPP

#include <string>

class Parameter{
public: 
    std::string name;
    std::string help;
    bool isRequired;

    Parameter(std::string name, std::string help, bool isRequired): name{name}, help{help}, isRequired{isRequired} {};
    ~Parameter(){};
};

#endif // PARAMETER_HPP
