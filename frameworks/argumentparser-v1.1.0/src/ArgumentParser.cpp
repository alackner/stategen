#include "ArgumentParser.hpp"
#include <optional>
#include <stdlib.h>

auto defaultExit = [](){
    exit(0);
};

ArgumentParser::ArgumentParser() :
    description{""},
    infoStream{std::cout},
    exitFunction{defaultExit}
{

}

ArgumentParser::ArgumentParser(std::string description) : description{description},
    infoStream{std::cout},
    exitFunction{defaultExit}
{

}

ArgumentParser::ArgumentParser(std::string description, std::ostream& infoStream, std::function<void()> exitFunction) :
    description{description},
    infoStream{infoStream},
    exitFunction{exitFunction}
{

}

ArgumentParser::~ArgumentParser()
{

}

void ArgumentParser::addArgument(std::string parameter, std::string help, bool required)
{
    parameters.push_back(Parameter(parameter, help, required));
}

void ArgumentParser::parse(int argc, char** argv)
{
    std::string capturedArg = "";
    parsedArguments.clear();

    for(int argvIndex=1; argvIndex < argc; argvIndex++)
    {
        std::string cArg = std::string{argv[argvIndex]};

        if(cArg.rfind("-", 0) == 0)
        {
            if(cArg.compare("-h") == 0)
            {
                printHelp();
            }

            auto param = std::find_if(parameters.begin(), parameters.end(), [&](Parameter& param){
                return (cArg.compare(param.name) == 0);
            });

            if(param != parameters.end())
            {
                int valueIndex = argvIndex + 1;

                if(valueIndex < argc)
                {
                    parsedArguments.insert(std::pair{cArg, std::string{argv[valueIndex]}});
                    argvIndex++;
                }
                else {
                    infoStream << "No value found for parameter " << cArg << "." << std::endl;
                    exitFunction();
                }
            }
        }
    }

    checkIfRequiredExist();
}

std::optional<std::string> ArgumentParser::getArgument(std::string parameter)
{
    auto pResult = parsedArguments.find(parameter);

    if(pResult != parsedArguments.end())
    {
        return pResult->second;
    }

    return std::nullopt;
}

void ArgumentParser::checkIfRequiredExist()
{
    for(auto param : parameters)
    {
        if(param.isRequired)
        {
            if(getArgument(param.name) == std::nullopt)
            {
                infoStream << "Value for required parameter " << param.name << " is missing." << std::endl;
                exitFunction();
            }
        }
    }
}

void ArgumentParser::printHelp()
{
    infoStream << description << std::endl << std::endl;

    for(auto param : parameters)
    {
       infoStream << param.name << "\t" << param.help << std::endl;
    }

    exitFunction();
}